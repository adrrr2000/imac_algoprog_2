#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return 2*nodeIndex+1;
}

int Heap::rightChild(int nodeIndex)
{
    return 2*nodeIndex+2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i

	int i = heapSize;
	this->get(i)=value;
	while(i>0 && this->get(i) > this->get((i-1)/2)){
		this->swap(i,(i-1)/2);
		i=(i-1)/2;
	}
	
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i

	int i_max = nodeIndex;
	int i = nodeIndex;

	int i_gauche = this->leftChild(i);
	int i_droite = this->rightChild(i);

	if(i_gauche<heapSize){
		if(this->get(i_max)<this->get(i_gauche)){
		i_max = i_gauche;
	}
	}

	if(i_droite<heapSize){
		if(this->get(i_max)<this->get(i_droite)){
		i_max = i_droite;
	}
	}


	if(i_max!=i){
		this->swap(i_max,i);
		this->heapify(heapSize,i_max);
	}

}

void Heap::buildHeap(Array& numbers)
{
	int taille = numbers.size();
	for (int i = 0; i < taille; i++)
	{
		insertHeapNode(i, numbers[i]);
	}
	
}

void Heap::heapSort()
{
	for (int i = size()-1; i>=0; i--){
    	swap(0,i);
        heapify(i, 0);
    }

}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
