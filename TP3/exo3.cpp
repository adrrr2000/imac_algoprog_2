#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->value = value;
        this->left = NULL;
        this->right = NULL;
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child
        if (value<this->value)
        {
            
            if (this->left == NULL)
            {
                this->left = new SearchTreeNode(value);
            }
            else
            {
                this->left->insertNumber(value);
            }
        }
        else
        {
            if (this->right == NULL)
            {
                this->right = new SearchTreeNode(value);
            }
            else
            {
                this->right->insertNumber(value);
            }
        }
        
        
    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        if (this->left==NULL && this->right==NULL)
        {
            return 1;
        }
        int hauteurGauche = 0;
        int hauteurDroite = 0;
        if (left!=NULL) //on peut écrire juste left à la place de this->left
        {
            hauteurGauche = left->height();
        }
        if (right!=NULL) 
        {
            hauteurDroite = right->height();
        }
        return std::max(hauteurGauche,hauteurDroite) + 1; //si ça marche pas ajouter std:: devant max;
        
    

    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        if (this->left==NULL && this->right==NULL) //on utilisera isLeaf() pour vérifier cette condition par la suite
        {
            return 1;
        }
        int compteGauche = (left==NULL) ? 0 : left->nodesCount(); //cette ligne équivaut au commentaire ci-dessous -> int nombre = (condition) ? valeur1 : valeur2;
        
        // compteGauche = 0;
        // if (left!=NULL) 
        // {
        //     compteGauche = left->nodesCount();
        // }
        int compteDroit = (right==NULL) ? 0:right->nodesCount();

        // compteDroit = 0;
        // if (right!=NULL) 
        // {
        //     compteDroit = right->nodesCount();
        // }

        return compteGauche + compteDroit + 1;
	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        return left==NULL && right==NULL;
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree

        //faut mettre que les feuilles pas tout l'arbre
        if (isLeaf())
        {
            leaves[leavesCount]=this;
            leavesCount++;
        }

        if (right!=NULL)
        {
            right->allLeaves(leaves, leavesCount);
        }

        if (left!=NULL)
        {
            left->allLeaves(leaves, leavesCount);
        }
        
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        
        

        if (left!=NULL)
        {
            left->inorderTravel(nodes,nodesCount);
            
        }
        
        nodes[nodesCount] = this;
        nodesCount++;

        if (right!=NULL)
        {
            right->inorderTravel(nodes,nodesCount);
        }

        
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        nodes[nodesCount] = this;
        nodesCount++;
        
        if (left!=NULL)
        {
            left->preorderTravel(nodes,nodesCount);


        }

        if (right!=NULL)
        {
            right->preorderTravel(nodes,nodesCount);
        }

        
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        
        if (left!=NULL)
        {
            left->postorderTravel(nodes,nodesCount);


        }

        if (right!=NULL)
        {
            right->postorderTravel(nodes,nodesCount);
        }

        nodes[nodesCount] = this;
        nodesCount++;
	}

	Node* find(int value) {
        // find the node containing value
        if (this->value==value){
            return this;
        }
       
        else if (value < this->value && left!=NULL)
        {
            return left->find(value);
        }
        
        else if (value > this->value && right!=NULL)
        {
            return right->find(value);
        }
		return nullptr;
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}
