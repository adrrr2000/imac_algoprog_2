#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    // your code
    Noeud* dernier;
};

struct DynaTableau{
    int* donnees;
    // your code
    int nbElements;
    int capacite;


};


void initialise(Liste* liste)
{
    liste->premier = NULL;
    liste->dernier = NULL;
}

bool est_vide(const Liste* liste)
{
    return liste->premier==NULL;
}

void ajoute(Liste* liste, int valeur)
{
    Noeud* noeud = new Noeud;
    noeud->donnee=valeur;
    if (est_vide(liste))
    {
        liste->premier = noeud;
        liste->dernier = noeud;
    }
    else{
        liste->dernier->suivant = noeud;
        liste->dernier = noeud;
    }
    
}

void affiche(const Liste* liste)
{   
    if (est_vide(liste))
    {
        cout<<"La liste est vide"<<endl;
    }
    
    Noeud* noeudActuel = liste->premier;
    while (noeudActuel != liste->dernier)
    {
        cout << noeudActuel->donnee << endl;
        noeudActuel = noeudActuel->suivant;

    }
    
}

int recupere(const Liste* liste, int n) // n doit être >=1 
{
    if (n<1)
    {
        cout << "indice trop petit" << endl;
        return -1;
    }
    
    Noeud* noeudActuel = liste->premier; 
    for (int i = 0; i < n-1; i++)
    {
        noeudActuel = noeudActuel->suivant;
        if (noeudActuel == NULL)
        {
            cout<< "Il n'y pas assez d'élements dans la liste" << endl;
            return -1;
        }
        
    }
    cout << noeudActuel->donnee<<endl;
    return 0;
}

int cherche(const Liste* liste, int valeur)
{
    int index = 0;
    Noeud* noeudActuel = liste->premier; 
    while (noeudActuel->donnee != valeur)
    {
        noeudActuel = noeudActuel->suivant;
        index++;
        if (noeudActuel == NULL)
        {
            return -1;
        } 
    }
    return index;
}

void stocke(Liste* liste, int n, int valeur)
{
    int index = 0;
    Noeud* noeudActuel = liste->premier; 
    while (index != n)
    {
        noeudActuel = noeudActuel->suivant;
        index++;
        if (noeudActuel == NULL)
        {
            cout << "pas assez d'éléments" << endl;
        } 
    }
    noeudActuel->donnee = valeur;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if (tableau->nbElements==tableau->capacite)
    {
        tableau->capacite*=2;
        tableau->donnees = (int*) realloc (tableau->donnees, sizeof(int) * tableau->capacite);
    }
    tableau->donnees[tableau->nbElements] = valeur;
    tableau->nbElements++;
    
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->capacite=capacite;
    tableau->nbElements=0;
    tableau->donnees=(int*)malloc(sizeof(int)*capacite);



}

bool est_vide(const DynaTableau* liste)
{
    return liste->nbElements == 0;
    
}

void affiche(const DynaTableau* tableau)
{
    if (est_vide(tableau))
    {
        cout <<"le tableau est vide";
    }
    else
    {
        for (int i = 0; i < tableau->nbElements; i++)
        {
            cout<<tableau->donnees[i]<<"; ";
        }
    }
    cout << endl;
    
    
}

int recupere(const DynaTableau* tableau, int n) // n doit être > 0
{
    if (n>tableau->nbElements)
    {
        cout << "pas assez d'éléments"<<endl;
    }
    else
    {
        cout<<tableau->donnees[n-1]<<endl;
    }
    return 0;
}

int cherche(const DynaTableau* tableau, int valeur)
{
    int i = 0;
    while (i<tableau->nbElements)
    {
        if (tableau->donnees[i] == valeur)
        {
            return i;
        }
        i++;
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur) // je considère que la n-ème valeur correspond à l'indice n-1 (ie. je compte à partir de 1)
{
    if (n>tableau->nbElements)
    {
        cout << "pas assez d'éléments"<<endl;
        return;
    }
    
    tableau->donnees[n-1]= valeur;
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(DynaTableau* liste, int valeur) // FIFO 
{
    ajoute(liste, valeur);
}

//int retire_file(DynaTableau* liste)
int retire_file(DynaTableau* liste)
{
    int valeur = liste->donnees[0];
    for (int i = 0; i < liste->nbElements-1; i++)
    {
        liste->donnees[i]=liste->donnees[i+1];
    }
    liste->donnees[liste->nbElements] = NULL;
    liste->nbElements--;
    return valeur;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(DynaTableau* liste, int valeur)
{
    ajoute(liste, valeur);
}

//int retire_pile(DynaTableau* liste)
int retire_pile(DynaTableau* liste)
{
    int valeur = liste->donnees[liste->nbElements];
    liste->donnees[liste->nbElements] = NULL;
    liste->nbElements--;
    return valeur;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    // Liste pile; 
    // Liste file; 
    DynaTableau pile;
    DynaTableau file;

    // initialise(&pile);
    // initialise(&file);

    initialise(&pile,10);
    initialise(&file,10);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
