#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){
    // recursiv Mandelbrot
    

    float x= z.x*z.x - z.y*z.y + point.x;
    float y= 2*z.x*z.y + point.y;
    Point a(x,y);
    
    if (a.length()>2)
    {
        return n;
    }
    if (n<1)
    {
        return 0;
    }
    
    return(isMandelbrot(a,n-1,point));
    

    // check length of z
    // if Mandelbrot, return 1 or n (check the difference)
    // otherwise, process the square of z and recall
    // isMandebrot
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



