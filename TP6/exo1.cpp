#include "tp6.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;

void Graph::buildFromAdjenciesMatrix(int **adjacencies, int nodeCount)
{
	/**
	  * Make a graph from a matrix
	  * first create all nodes, add it to the graph then connect them
	  * this->appendNewNode
	  * this->nodes[i]->appendNewEdge
	  */
	for (int i = 0; i < nodeCount; i++) // on crée un noeud et sa valeur correspond à son indice
	{
		GraphNode* node = new GraphNode(i); //on crée un nod (du graph) avec la valeur i
		this->appendNewNode(node); // on ajoute ce nod au tableau de nod du graph (c'est nodes)
	}
	
	for (size_t i = 0; i < nodeCount; i++)
	{
		for (size_t j = 0; j < nodeCount; j++) { // on parcourt le tableau 2D qui donne les liaisons entre les noeuds
			
			if (adjacencies[i][j]!=0) //si la case est différente de 0, c'est qu'il y a une liaison
			{
				this->nodes[i]->appendNewEdge(this->nodes[j],adjacencies[i][j]); // on ajoute l'arrête au noeud i, qui a pour destination le noeud j et pour distance adjacencies[i][j]
			}
			
		}

	}
	
	
	
}

void Graph::deepTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	  * Fill nodes array by travelling graph starting from first and using recursivity
	  */
	
	/** rempli le tableau nodes en parcourant le graphe en profondeur à partir de first. nodeSize est le nombre de noeud dans nodes et est donc égale à 0 lors du premier appel de fonction. visited est un tableau de booléen rempli de false lors du premier appel.**/

	nodes[nodesSize]= first; //si le tableau est vide (taille 0), on remplit l'indice 0. S'il a une taille de 1, on remplit l'indice 1 etc.

	visited[first->value]=true; //on a dit que la value correspond à l'indice du noeud Donc ici on sait qu'on est passé par le noeud donc on a recupéré l'indice grâce à sa valeur

	nodesSize++; // on incrémente la taille du tableau qu'on remplit

	for (Edge* e = first->edges; e!=NULL; e=e->next) //on parcourt toutes les arrêtes du noeud
	{
		if (!visited[e->destination->value]) //ça correspond à if(visited[e->destination->value]!=true)
		//en gros on regarde si le sommet de destination de l'arrête a déjà été parcouru. Si ce n'est pas le cas, on rappelle la fonction sur ce sommet 
		{
			deepTravel(e->destination, nodes, nodesSize, visited); 
		}
		
	}
}

void Graph::wideTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	 * Fill nodes array by travelling graph starting from first and using queue
	 * nodeQueue.push(a_node)
	 * nodeQueue.front() -> first node of the queue
	 * nodeQueue.pop() -> remove first node of the queue
	 * nodeQueue.size() -> size of the queue
	 */
	std::queue<GraphNode*> nodeQueue; // on définit une file
	nodeQueue.push(first); // on lui ajoute le premier sommet 
	
	while (!nodeQueue.empty()) //tant que la file n'est pas vide
	{
		GraphNode* sommet = nodeQueue.front(); //on récupère le 1er sommet de la file
		nodes[nodesSize]= sommet; //on met ce sommet dans le tableau
		nodeQueue.pop(); //on retire le 1er sommet 
		visited[sommet->value]=true; // on note qu'on a récupéré ce sommet
		nodesSize++; // on incrémente la taille du tableau qu'on remplit

		for (Edge* e = sommet->edges; e!=NULL; e=e->next) // //on parcourt toutes les arrêtes du sommet
		{
			if (!visited[e->destination->value]) // on regarde si le sommet de destination de l'arrête a déjà été parcouru. Si ce n'est pas le cas, on l'ajoute à la file
			{
				nodeQueue.push(e->destination);
			}
		}
	}
	
}

bool Graph::detectCycle(GraphNode *first, bool visited[])
{
	/**
	  Detect if there is cycle when starting from first
	  (the first may not be in the cycle)
	  Think about what's happen when you get an already visited node
	**/

	// je vais utiliser le même principe que pour deepTravel, donc la récursivité

	visited[first->value]=true; //on dit qu'on est passé par le 1er sommet 

	for (Edge* e = first->edges; e!=NULL; e=e->next) //on parcourt toutes les arrêtes du noeud
	{
		if (!visited[e->destination->value]) //si le sommet de destination n'a pas été visité, on continue le chemin
            detectCycle(e->destination, visited);
        else
            return true; // sinon on renvoie true

	}

    return false;
}



int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 150;
	w = new GraphWindow();
	w->show();
	return a.exec();
}
