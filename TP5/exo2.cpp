#include <QApplication>
#include <QString>
#include <time.h>
#include <stdio.h>
#include <string>

#include <tp5.h>

#include <math.h> //pour la fonction puissance

MainWindow* w = nullptr;
using std::size_t;
using std::string;


std::vector<string> TP5::names(
{
    "Yolo", "Anastasiya", "Clement", "Sirine", "Julien", "Sacha", "Leo", "Margot",
    "JoLeClodo", "Anais", "Jolan", "Marie", "Cindy", "Flavien", "Tanguy", "Audrey",
    "Mr.PeanutButter", "Bojack", "Mugiwara", "Sully", "Solem",
    "Leo", "Nils", "Vincent", "Paul", "Zoe", "Julien", "Matteo",
    "Fanny", "Jeanne", "Elo"
});

unsigned long int hash(string key)
{
    // return an unique hash id from key
    //par exemple pour écire 'yo' on met 'y'*128^1 + 'o'*128^0 en remplaçant les lettres par les codes ascii

    int size=key.size();
    unsigned long int id=0; // pour les entiers grands positifs
    int j =0; //indice pour parcourir la chaine de caractères
    for (int i = size-2; i >-1; i--)
    {
        id+=(int)key[j]*pow(128,i); //la première lettre prend la plus grande puissance, c'est pour ça qu'on décrémente dans la boucle
        j++;
    }
    qDebug("%d", id); // le prof m'ajouté ça psk ça bugait, je pourrai peut-être le retirer à la fin
    return id;
}

struct MapNode : public BinaryTree
{

    string key;
    unsigned long int key_hash;

    int value;

    MapNode* left;
    MapNode* right;

    MapNode(string key, int value) : BinaryTree (value)
    {
        this->key = key;
        this->value = value;
        this->key_hash = hash(key);

        this->left = this->right = nullptr;
    }

    /**
     * @brief insertNode insert a new node according to the key hash
     * @param node
     */
    void insertNode(MapNode* node)
    {
        // on a repris l'exo3 du tp3 
        
        if (node->key_hash < this->key_hash) //on compare la hash key avec celle du noeud actuel, si elle est plus petite, on part à gauche
        {
            
            if (this->left == NULL)
            {
                this->left = node;
            }
            else
            {
                this->left->insertNode(node);
            }
        }
        else
        {
            if (this->right == NULL)
            {
                this->right = node;
            }
            else
            {
                this->right->insertNode(node);
            }
        }
    }

    void insertNode(string key, int value)
    {
        this->insertNode(new MapNode(key, value));
    }

    virtual ~MapNode() {}
    QString toString() const override {return QString("%1:\n%2").arg(QString::fromStdString(key)).arg(value);}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

struct Map
{
    Map() {
        this->root = nullptr;
    }

    /**
     * @brief insert create a node and insert it to the map
     * @param key
     * @param value
     */
    void insert(string key, int value)
    {
        if (this->root==nullptr)
        {
            this->root=new MapNode(key, value);
        }
        else
        {
            this->root->insertNode(key, value);
        }
        
        

        
    }

    /**
     * @brief get return the value of the node corresponding to key
     * @param key
     * @return
     */
    int get(string key)
    {
        MapNode* currentNode = this->root; //on stocke le noeud qui va être comparé à la valeur recherchée (donc on prend la racine au début)
        
        while (currentNode!=nullptr)
        {
            if (hash(key) ==currentNode->key_hash) // si a trouvé, on renvoie la valeur
            {
                return currentNode->value;
            }
        
            if (hash(key) < currentNode->key_hash)
            {
                currentNode = currentNode->left;
            
            }
            else if (hash(key) > currentNode->key_hash)
            {
                currentNode = currentNode->right;
                
            }
        }

        return 0; // on renvoie 0 si on a rien trouvé
    }

    MapNode* root;
};


int main(int argc, char *argv[])
{
    srand(time(NULL));
	Map map;

    map.insert("Yolo", 20);
    for (std::string& name : TP5::names)
    {
        if (rand() % 3 == 0)
        {
            map.insert(name, rand() % 21);
        }
    }

    printf("map[\"Margot\"]=%d\n", map.get("Margot"));
    printf("map[\"Jolan\"]=%d\n", map.get("Jolan"));
    printf("map[\"Lucas\"]=%d\n", map.get("Lucas"));
    printf("map[\"Clemence\"]=%d\n", map.get("Clemence"));
    printf("map[\"Yolo\"]=%d\n", map.get("Yolo"));
    printf("map[\"Tanguy\"]=%d\n", map.get("Tanguy"));


    QApplication a(argc, argv);
    MainWindow::instruction_duration = 200;
    w = new MapWindow(*map.root);
    w->show();
    return a.exec();
}
