#include <tp5.h>
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;


std::vector<string> TP5::names(
{
    "Yolo", "Anastasiya", "Clement", "Sirine", "Julien", "Sacha", "Leo", "Margot",
    "JoLeClodo", "Anais", "Jolan", "Marie", "Cindy", "Flavien", "Tanguy", "Audrey",
    "Mr.PeanutButter", "Bojack", "Mugiwara", "Sully", "Solem",
    "Leo", "Nils", "Vincent", "Paul", "Zoe", "Julien", "Matteo",
    "Fanny", "Jeanne", "Elo"
});


int HashTable::hash(std::string element)
{
    // use this->size() to get HashTable size
    int size=this->size();
    int hash_value = (int)element[0]%size;
    //on aurait pu mettre la ligne de code ci dessous, mais en fait en fait si la size est plus grande, ça renvoie bien juste le code ascii de l'element 
    //int hash_value = ((int)element[0]<size) ? (int)element[0] : (int)element[0]%size;
    return hash_value;
}

void HashTable::insert(std::string element)
{
    // use (*this)[i] or this->get(i) to get a value at index i
    int i=this->hash(element); // on récupère la hash value avec la fonction précédente
    (*this)[i]= element; 

}

/**
 * @brief buildHashTable: fill the HashTable with given names
 * @param table table to fiil
 * @param names array of names to insert
 * @param namesCount size of names array
 */
void buildHashTable(HashTable& table, std::string* names, int namesCount)
{
    for (int i = 0; i < namesCount; i++)
    {
        table.insert(names[i]); //on met un point et pas une -> psk table c'est pas un pointeur mais une référence
    }
}

bool HashTable::contains(std::string element)
{
    // Note: Do not use iteration (for, while, ...)
    int i=this->hash(element);
    return (*this)[i]==element;
    
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 10;
	w = new HashWindow();
	w->show();

	return a.exec();
}
